# 4A-G pistons
I did some metrology on some 4A-G pistons. I have an 81.5 mm OEM AE111 piston and an 81 mm OEM AE101 piston.

First thing I did was scan the pistons using a laser scanning head on a Mitutoyo CMM. This created a point cloud which I turned into an STL mesh.

Next I took each piston and did some quick contact contour measurements using a Mitutoyo contracer. This provides a line across the piston. I did one line through the exhaust pocket on the AE111 piston. On the AE101 piston I did a line through the exhaust pocket and through the center intake valve pocket.

My next intention is to do touch probe measurements to be able to reconstruct the piston tops in a CAD package.

# License
The information in this repository is licensed under a CC0 Creative Commons Public Domain license.
